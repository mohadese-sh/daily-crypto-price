import React from 'react';
import styles from "../components/Coin.module.css";

import up from "../gif/arrow-up.png";
import down from "../gif/arrow-down.png";

const Coin = ({name, image, price, marketCap, symbol, priceChange}) => {
    return (
        <div className={styles.container}>
            <img className={styles.image} src={image} alt={name} />
            <span className={styles.name}>{name}</span>
            <span className={styles.symbol}>{symbol.toUpperCase()}</span>
            <span className={styles.price}>$ {price.toLocaleString()}</span>
            <span className={priceChange>0 ? styles.priceUp : styles.priceDown}>{priceChange.toFixed(2)}<img className={styles.imageUp} src={priceChange > 0 ? up : down} alt="pricechange" /></span>
            <span className={styles.symbol}>{marketCap.toLocaleString()}</span>
        </div>
    );
};

export default Coin;