import React, { useEffect, useState } from 'react';
import Loader from './Loader';
import { getCoin2 } from '../services/api';
import Coin from './Coin';
import styles from "../components/Landing.module.css";

const Landing = () => {
    const [coins, setCoins] = useState([]);
    const [search, setSearch] = useState("");

    useEffect(() => {
        const fetchAPI = async () => {
            const data = await getCoin2();
            setCoins(data);
            console.log(data);
        }
        fetchAPI();
    }, [])


    const changeHandler = (event) => {
        setSearch(event.target.value);
    }

    const searchedCoins = coins.filter(coin => coin.name.toLowerCase().includes(search.toLowerCase()))

    return (
        <>
            <input className={styles.input} type="text" placeholder='Search...' value={search} onChange={changeHandler} />
            {
                coins.length ? 
                <div className={styles.coinContainer}>
                    {
                        searchedCoins.map(coin => <Coin 
                            key= {coin.id}
                            image= {coin.image}
                            name= {coin.name}
                           symbol= {coin.symbol}
                            price= {coin.current_price}
                            marketCap = {coin.market_cap}
                            priceChange = {coin.price_change_percentage_24h}
                        />)
                    }
                </div>
                 : <Loader />
            }
            
            
        </>
    );
};

export default Landing;