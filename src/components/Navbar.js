import React from 'react';
import { Link } from 'react-router-dom';
import styles from "../components/Navbar.module.css";

const Navbar = () => {
    return (
        <div>
            <ul className={styles.container}>
                <li><Link to="/landing1">1</Link></li>
                <li><Link to="/landing2">2</Link></li>
            </ul>
        </div>
    );
};

export default Navbar;