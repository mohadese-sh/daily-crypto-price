import './App.css';
import Landing from './components/Landing';
import Landing2 from './components/Landing2';
import { Route, Routes, Navigate } from "react-router-dom"
import Navbar from './components/Navbar';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/landing1' element={<Landing/>}/>
        <Route path='/landing2' element={<Landing2/>}/>
        <Route path='/*' element={<Navigate to='/landing1' />} />
      </Routes>
      <Navbar />
    </div>
  );
}

export default App;
